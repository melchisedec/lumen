<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\Models\Taxonomies;

class Taxonomy {

	public $id;
	public $name = [];

	public function __construct( $identifier ) {
		$record = Taxonomies::where(
			( is_numeric( $identifier ) ? 'id' : 'singular' ), '=', $identifier
		)->first();

		if ( !empty( $record ) ) :

			$this->id = $record->id;
			$this->name = [
				'singular' => $record->singular,
				'plural' => $record->plural
			];

		endif;
	}

	public static function add( $singular = '', $plural = '' ) {
		if ( empty( $singular ) || empty( $plural ) )
			return null;

		$taxonomy = new Taxonomy( $singular, $plural );

		if ( $taxonomy->id != null )
			return $taxonomy->id;

		$taxonomy = new Taxonomies();
		$taxonomy->singular = $singular;
		$taxonomy->plural = $plural;
		$taxonomy->save();

		return $taxonomy->id;
	}
	
	
	   public static function get_taxonomies( $identifier ) {

        $records = DB::select( "SELECT id, singular, plural FROM taxonomies WHERE singular = :value ORDER BY id ASC LIMIT 1", [
            'value' => $identifier
        ] );

        return !empty( $records ) ? $records : [];
    }


}