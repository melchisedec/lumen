<?php

namespace App\Http\Controllers\Accounts;

use Illuminate\Support\Facades\Input;

use App\Entities\Account;
use App\Http\Controllers\Controller;

class AccountController extends Controller {

    public function validate_account() {
        $account = new Account( Input::get( 'account', '' ) );

        return response()->json( [
            'result' => $account->id == null ? 'failed' : 'successful',
            'account_id' => $account->id
        ] );
    }

}