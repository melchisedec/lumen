<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model {

    protected $table = 'terms';
    protected $primaryKey = 'id';
    protected $fillable = [ 'taxonomy_id', 'user_id', 'name' ];
    protected $hidden = [ 'taxonomy_id',  'created_at', 'updated_at' ];

}