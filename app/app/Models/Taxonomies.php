<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taxonomies extends Model {

    protected $table = 'taxonomies';
    protected $primaryKey = 'id';
    protected $fillable = [ 'singular', 'plural' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

}