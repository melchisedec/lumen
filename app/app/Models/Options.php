<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Options extends Model {

    protected $table = 'options';
    protected $primaryKey = 'id';
    protected $fillable = [ 'name', 'value', 'auto_load' ];
    protected $hidden = [ 'auto_load', 'created_at', 'updated_at' ];

}