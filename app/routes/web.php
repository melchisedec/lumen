<?php

$app->group( [ 'prefix' => '', 'middleware' => 'APICors' ], function () use ( $app ) {
    $app->post('/account/validate', [
        'uses' => 'Accounts\AccountController@validate_account'
    ]);

    $app->post('/users', [
        'uses' => 'Users\UserController@index'
    ]);

    $app->post('/modules', [
        'uses' => 'Modules\ModuleController@index'
    ]);
} );