<?php

use Illuminate\Database\Seeder;

use App\Entities\Taxonomy;
use App\Entities\Module;
use App\Entities\Account;
use App\Entities\TermMeta;

class ModulesSeeder extends Seeder  {

    public function run() {
	    Taxonomy::add( 'Module', 'Modules' );

        $metajua = new Account( 'Metajua' );

        $entries = [
            [
                'name' => 'Module 1',
                'account' => $metajua->id
            ],
            [
                'name' => 'Module 2',
                'account' => $metajua->id
            ],
	        [
		        'name' => 'Module 3',
		        'account' => $metajua->id
	        ],
            [
                'name' => 'Stats',
                'account' => $metajua->id
            ]
        ];

        foreach( $entries as $entry ) :

            $entry = ( object ) $entry;

            $module = new Module( Module::add( $entry->name ) );

            isset( $entry->account ) ? TermMeta::add( $module->id, 'account', $entry->account ) : null;

        endforeach;
    }

}
