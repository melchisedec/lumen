<?php

use Illuminate\Database\Seeder;

use App\Models\Option;

class OptionsTableSeeder extends Seeder {

    public function run() {
        $options = [
            'product' => [
                'name' => 'Metajua App',
                'version' => '0.0.0.1'
            ]
        ];

        foreach( $options[ 'product' ] as $key => $value ) :

            $option = new Option();

            $option->name = 'product_' . $key;
            $option->value = $value;
            $option->auto_load = true;

            $option->save();

        endforeach;
    }

}
